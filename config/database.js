const mysql = require('mysql');

const db = mysql.createConnection({
  host: process.env.db_host,
  port: process.env.db_port,
  user: process.env.db_user,
  password: process.env.db_password,
  database: process.env.db_name
});

db.connect((err) => {
  console.info('Connecting to the database...');

  if (err) {
    console.log('Error in connection');
    throw err;
  }
  console.log('Connected to the database');

});

module.exports = db;