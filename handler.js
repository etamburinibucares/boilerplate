module.exports.secured = async () => {
    console.log("Called method: secured");

    return {
        statusCode: 200,
        body: JSON.stringify({secured: true}, null,2)
    };
};

module.exports.version = async () => {
    console.log("Called method: version");

    const version = process.env.version;
    return {
        statusCode: 200,
        body: JSON.stringify({ version }, null, 2)
    };
};
