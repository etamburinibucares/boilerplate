module.exports.NotFound = (message) => {
    console.log(message);

    return ({
        statusCode: 404,
        body: JSON.stringify({
            message: message,
        })
    })
};

module.exports.InternalError= (message, err) => {
    console.log(err);
    console.log(message);

    return ({
        statusCode: 500,
        body: JSON.stringify({
            message: message,
        })
    })
};