const {NotFound, InternalError} = require('./exceptions');

let database = require('./config/database');
const { v4: uuidv4 } = require('uuid');

module.exports.getOrderByPublicId = async event => {
    console.log("Called method: getOrderByPublicId");

    return new Promise((resolve, reject) => {
        const sql = "SELECT public_id, status, create_date, purchase_date, reference_number, electronically_signed FROM `order` WHERE public_id = ?";

        const params = [event.pathParameters.id];

        console.log("Running:", sql);
        console.log("With parameters:", params);

        database.query(sql, params, function(err, row){
            if (err){
                resolve(InternalError(`Error processing the request`, err));
                console.log("Leaving: getOrderByPublicId");
                return;
            }
            console.log("Query result:", row);

            let result;

            if(row.length === 0){
                result = NotFound(`Order not found by public_id {${event.pathParameters.id}}`);
            }else{
                result = {
                    statusCode: 200,
                    body: JSON.stringify(row, null, 2)
                };
            }

            console.log("Leaving: getOrderByPublicId");
            resolve(result);
        })
    })
};

module.exports.getOrderByPublicIdDetails = async event => {
    console.log("Called method: getOrderByPublicIdDetails");

    return new Promise((resolve, reject) => {
        const sql = "SELECT * FROM `order` WHERE public_id = ?";

        const params = [event.pathParameters.id];

        console.log("Running: ", sql);
        console.log("With parameters: ", params);

        database.query(sql, params, function(err, row){
            if (err){
                resolve(InternalError(`Error processing the request`, err));
                console.log("Leaving: getOrderByPublicIdDetails");
                return;
            }

            console.log("Query result:", row);

            let result;

            if(row.length === 0){
                result = NotFound(`Order not found by public_id {${event.pathParameters.id}}`);

            }else{
                result = {
                    statusCode: 200,
                    body: JSON.stringify(row, null, 2)
                };
            }

            console.log("Leaving: getOrderByPublicIdDetails");
            resolve(result);
        })
    })
};

module.exports.getOrdersByUserPublicId = async event => {
    console.log("Called method: getOrdersByUserPublicId");

    return new Promise((resolve, reject) => {
        const sql = "SELECT public_id, status, create_date, purchase_date, reference_number, electronically_signed FROM `order` WHERE user_id = ?";

        const params = [event.pathParameters.id];

        console.log("Running: ", sql);
        console.log("With parameters: ", params);

        database.query(sql, params, function(err, row){
            if (err){
                resolve(InternalError(`Error processing the request`, err));
                console.log("Leaving: getOrdersByUserPublicId");
                return;
            }
            console.log("Query result:", row);

            let result;

            if(row.length === 0){
                result = NotFound(`Orders not found by user_id {${event.pathParameters.id}}`);

            }else{
                result = {
                    statusCode: 200,
                    body: JSON.stringify(row, null, 2)
                };
            }

            console.log("Leaving: getOrdersByUserPublicId");
            resolve(result);
        })
    })
};

module.exports.createOrder = async event => {
    console.log("Called method: createOrder");
    const statusStarted = process.env.status_started;

    return new Promise((resolve, reject) => {
        const body = JSON.parse(event.body);
        const sql = `INSERT INTO ${'`order`'} 
        (
          public_id, 
          product_id, 
          user_id,
          partner_id,
          status,
          reference_number
          ${body.creator_id ? ', creator_id' : ''} 
          ${body.is_creator_testamento_user ? ', is_creator_testamento_user' : ''} 
        )
        VALUES (?, ?, ?, ?, ?, ? ${body.creator_id ? ', ?' : ''} ${body.is_creator_testamento_user ? ', ?' : ''} );`;

        const params = [
            uuidv4(),
            body.product_id,
            body.user_id,
            body.partner_id,
            statusStarted,
            body.reference_number,
        ];

        body.creator_id ? params.push(body.creator_id) : null;
        body.is_creator_testamento_user ? params.push(body.is_creator_testamento_user) : null;

        console.log("Running:", sql);
        console.log("With parameters:", params);

        database.query(sql, params, function(err, insertion){
            if (err){
                resolve(InternalError(`Error processing the request`, err));
                console.log("Leaving: createOrder");
                return;
            }

            console.log("Insertion result:", insertion);

            const sql2 = "SELECT public_id, status, create_date, purchase_date, reference_number, electronically_signed FROM `order` WHERE id = ?";

            const params2 = [insertion.insertId];

            console.log("Running:", sql2);
            console.log("With parameters:", params2);

            database.query(sql2, params2, function(err2, row){
                if (err2){
                    resolve(InternalError(`Error processing the request`, err2));
                    console.log("Leaving: createOrder");
                    return;
                }

                console.log("Query result:", row);

                const result = {
                    statusCode: 200,
                    body: JSON.stringify(row, null, 2)
                };

                console.log("Leaving: createOrder");
                resolve(result);
            })
        })
    })
};

