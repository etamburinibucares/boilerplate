const handler = require('../handler');

test('correct response is generated in version function', () => {
	handler.version()
		.then((response) => {
			expect(response.statusCode).toEqual(200);
		});
});

test('correct body is generated in version function', () => {
  const version = process.env.version;

  handler.version()
		.then((response) => {
			expect(JSON.parse(response.body).version).toEqual(version);
		});
});

test('correct response is generated in secured function', () => {
	handler.secured()
		.then((response) => {
			expect(response.statusCode).toEqual(200);
		});
});

test('correct body is generated in version function', () => {
  handler.secured()
		.then((response) => {
			expect(JSON.parse(response.body).secured).toEqual(true);
		});
});