const sinon = require('sinon');
let mock = sinon.mock(require('mysql'));
const {NotFound, InternalError} = require('../exceptions');
let selectResult = [];
let insertResult = [];
let selectErr = 'err';
let insertErr = 'err';

const id = 'db2b5998-c3e2-4c6f-b9ce-926a6d6a4984asd';

describe('Api test', function() {
    beforeEach(() => {
        mock.expects('createConnection').returns({
            connect: () => {
                console.log('Successfully connected');
            },
            query: (query, vars, callback) => {
                if(query.includes('INSERT')) {
                    callback(insertErr, insertResult);
                } else {
                    callback(selectErr, selectResult);
                }
            },
            end: () => {
                console.log('Connection ended');
            }
        });
    });

    afterEach(() => {
        mock.restore();
    });

    describe('Get Order By Public Id test', function() {

        it('A database problem generate an exception', function() {
            selectResult = null;
            selectErr = 'err';

            const expectResult = InternalError(`Error processing the request`, selectErr);

            const api = require('../api');
            return api.getOrderByPublicId({
                pathParameters: {id}
            }).then((result) => {
                expect(expectResult).toEqual(result);
            });
        });

        it('A request to an existing order returns that order', function(){
            selectResult = [
                {
                    "public_id": id,
                    "status": "1",
                    "create_date": "2020-03-05T00:56:03.000Z",
                    "purchase_date": null,
                    "reference_number": "generali",
                    "electronically_signed": null
                }
            ];
            selectErr = null;

            const expectResult = {
                statusCode: 200,
                body: selectResult
            };

            const api = require('../api');
            return api.getOrderByPublicId({
                pathParameters: {id}
            }).then((result) => {
                result.body = JSON.parse(result.body);
                expect(expectResult).toEqual(result);
            });
        });

        it('A request to an nonexistent order does not return an order', function(){
            selectResult = [];
            selectErr = null;
            const expectResult = {
                statusCode: 404,
                body: {
                    message: `Order not found by public_id {${id}}`
                }
            };

            const api = require('../api');
            return api.getOrderByPublicId({
                pathParameters: {id}
            }).then((result) => {
                result.body = JSON.parse(result.body);
                expect(expectResult).toEqual(result);
            });
        });
    });

    describe('Get Order By Public Id Details test', function() {
        it('A database problem generate an exception', function() {
            selectResult = null;
            selectErr = 'err';

            const expectResult = InternalError(`Error processing the request`, selectErr);

            const api = require('../api');
            return api.getOrderByPublicIdDetails({
                pathParameters: {
                    id,
                }
            }).then((result) => {
                expect(expectResult).toEqual(result);
            });
        });

        it('A request to an existing order returns that order', function(){
            selectResult = [
                {
                    "id": 1,
                    "public_id": id,
                    "product_id": 1,
                    "user_id": 1,
                    "result": "{}",
                    "payment_id": null,
                    "processor_id": null,
                    "partner_id": 1,
                    "creator_id": null,
                    "is_creator_testamento_user": null,
                    "status": "1",
                    "status_history": null,
                    "product_module": null,
                    "retractation_ack_date": null,
                    "create_date": "2020-01-01T00:00:00+00:00",
                    "update_date": "2020-01-01T00:00:00+00:00",
                    "reference_number": "test",
                    "purchase_date": null,
                    "electronically_signed": null
                }
            ];

            selectErr = null;

            const expectResult = {
                statusCode: 200,
                body: selectResult
            };

            const api = require('../api');
            return api.getOrderByPublicIdDetails({
                pathParameters: {id}
            }).then((result) => {
                result.body = JSON.parse(result.body);
                expect(expectResult).toEqual(result);
            });
        });

        it('A request to an nonexistent order does not return an order', function(){
            selectResult = [];
            selectErr = null;

            const expectResult = NotFound(`Order not found by public_id {${id}}`);

            const api = require('../api');
            return api.getOrderByPublicIdDetails({
                pathParameters: {id}
            }).then((result) => {
                expect(expectResult).toEqual(result);
            });
        });
    });

    describe('Get Orders By User Public Id test', function() {
        it('A database problem generate an exception', function() {
            selectResult = null;
            selectErr = 'err';

            const expectResult = InternalError(`Error processing the request`, selectErr);

            const api = require('../api');
            return api.getOrdersByUserPublicId({
                pathParameters: {id}
            }).then((result) => {
                expect(expectResult).toEqual(result);
            });
        });

        it('A request to an existing order returns that order', function(){
            const id2 = "8c77b782-a215-4602-a598-4622db5310b2";
            selectResult = [
                {
                    "public_id": id,
                    "status": "0",
                    "create_date": "2020-01-01T00:00:00+00:00",
                    "purchase_date": null,
                    "reference_number": "generali",
                    "electronically_signed": null
                },
                {
                    "public_id": id2,
                    "status": "1",
                    "create_date": "2020-01-02T00:00:00+00:00",
                    "purchase_date": null,
                    "reference_number": "test_reference",
                    "electronically_signed": null
                },
            ];

            selectErr = null;

            const expectResult = {
                statusCode: 200,
                body: selectResult
            };

            const api = require('../api');
            return api.getOrdersByUserPublicId({
                pathParameters: {
                    id
                }
            }).then((result) => {
                result.body = JSON.parse(result.body);
                expect(expectResult).toEqual(result);
            });
        });

        it('A request to an nonexistent order does not return an order', function(){
            selectResult = [];
            selectErr = null;

            const expectResult = NotFound(`Orders not found by user_id {${id}}`);

            const api = require('../api');
            return api.getOrdersByUserPublicId({
                pathParameters: {id}
            }).then((result) => {
                expect(expectResult).toEqual(result);
            });
        });
    });

    describe('Create Order test', function() {
        it('A database problem in insertion generate an exception', function() {
            insertResult = null;
            selectResult = null;
            selectErr = null;
            insertErr = 'err';

            const expectResult = InternalError(`Error processing the request`, selectErr);

            const body = {
                product_id: 1,
                user_id: 1,
                partner_id: 1,
                creator_id: "test",
                is_creator_testamento_user: true,
                reference_number: "test_reference"
            };

            const api = require('../api');
            return api.createOrder({
                body: JSON.stringify(body)
            }).then((result) => {
                expect(expectResult).toEqual(result);
            });
        });

        it('A database problem in select generate an exception', function() {
            insertResult = { insertId: 1 };
            selectResult = null;
            selectErr = 'err';
            insertErr = null;

            const expectResult = InternalError(`Error processing the request`, selectErr);

            const body = {
                product_id: 1,
                user_id: 1,
                partner_id: 1,
                creator_id: "test",
                is_creator_testamento_user: true,
                reference_number: "test_reference"
            };

            const api = require('../api');
            return api.createOrder({
                body: JSON.stringify(body)
            }).then((result) => {
                expect(expectResult).toEqual(result);
            });
        });

        it('A correct insertion returns that insertion', function() {
            insertResult = { insertId: 1 };
            selectResult = [
                {
                    status: "1",
                    create_date: "2020-01-01T00:00:00+00:00",
                    purchase_date: null,
                    reference_number: "test_reference",
                    electronically_signed: null
                }
            ];
            selectErr = null;
            insertErr = null;

            const expectResult = {
                statusCode: 200,
                body: selectResult
            };

            const body = {
                product_id: 1,
                user_id: 1,
                partner_id: 1,
                creator_id: "test",
                is_creator_testamento_user: true,
                reference_number: "test_reference"
            };

            const api = require('../api');
            return api.createOrder({
                body: JSON.stringify(body)
            }).then((result) => {
                result.body = JSON.parse(result.body);
                expect(expectResult).toEqual(result);
            });
        });

        it('A correct insertion without creator_id returns that insertion', function() {
            insertResult = { insertId: 1 };
            selectResult = [
                {
                    status: "1",
                    create_date: "2020-01-01T00:00:00+00:00",
                    purchase_date: null,
                    reference_number: "test_reference",
                    electronically_signed: null
                }
            ];
            selectErr = null;
            insertErr = null;

            const expectResult = {
                statusCode: 200,
                body: selectResult
            };

            const body = {
                product_id: 1,
                user_id: 1,
                partner_id: 1,
                is_creator_testamento_user: true,
                reference_number: "test_reference"
            };

            const api = require('../api');
            return api.createOrder({
                body: JSON.stringify(body)
            }).then((result) => {
                result.body = JSON.parse(result.body);
                expect(expectResult).toEqual(result);
            });
        });

        it('A correct insertion without is_creator_testamento_user returns that insertion', function() {
            insertResult = { insertId: 1 };
            selectResult = [
                {
                    status: "1",
                    create_date: "2020-01-01T00:00:00+00:00",
                    purchase_date: null,
                    reference_number: "test_reference",
                    electronically_signed: null
                }
            ];
            selectErr = null;
            insertErr = null;

            const expectResult = {
                statusCode: 200,
                body: selectResult
            };

            const body = {
                product_id: 1,
                user_id: 1,
                partner_id: 1,
                creator_id: "test",
                reference_number: "test"
            };

            const api = require('../api');
            return api.createOrder({
                body: JSON.stringify(body)
            }).then((result) => {
                result.body = JSON.parse(result.body);
                expect(expectResult).toEqual(result);
            });
        });
    })
});