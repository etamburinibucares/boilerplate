'use strict';

module.exports.checkPermission = function(event, context, callback) {
    console.log("Called method: checkPermission");

    let token = event.authorizationToken;

    validateToken(token, (err, decodedToken) => {

        if (err) {
            console.error("User Unauthorized");
            return callback('Unauthorized');
        }

        generateAuthContext(decodedToken, event.methodArn, context, function (err, policy) {
            if (policy != null) {
                console.error("User Authorized");
                return callback(null, policy);
            }

            console.error("User Unauthorized");
            return callback('Unauthorized');
        });
    });
};


const validateToken = function (token, callback) {
    console.log(`Called method: validateToken: ${token}`);

    let decode = token; // Remove this logic

    /** Logic validation token
     * decode = ... */

    /** If token not valid
     * return callback(error); */

    if(token === void 0){
        return callback("Invalid token");
    }

    return callback(null, decode);
};

const generateAuthContext = function (decodedToken, methodArn, context, callback) {
    console.log(`Called method: generateAuthContext: ${decodedToken}`);

    try {
        let effect = 'Deny';

        /* Decoding token logic */

        let authContext = {
            /** token information... */
        };

        effect = 'Allow';
        callback(null, generatePolicy('user', effect, methodArn, authContext))
    } catch (error) {

    }
};

let generatePolicy = function(principalId, effect, resource, authContext) {
    console.log(`Called method: generatePolicy`);

    let authResponse = {};

    authResponse.principalId = principalId;
    if (effect && resource) {
        let policyDocument = {};
        policyDocument.Version = '2012-10-17';
        policyDocument.Statement = [];

        let statementOne = {};
        statementOne.Action = 'execute-api:Invoke';
        statementOne.Effect = effect;
        statementOne.Resource = resource;

        policyDocument.Statement[0] = statementOne;
        authResponse.policyDocument = policyDocument;
    }

    authResponse.context = authContext;

    console.debug("Json policy:", JSON.stringify(authResponse));

    return authResponse;
};